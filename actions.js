export const actionTypes = {
  LOAD_DATA: 'LOAD_DATA',
  LOAD_DATA_SUCCESS: 'LOAD_DATA_SUCCESS',
  DELETE_DATA: 'DELETE_DATA',
  DELETE_DATA_SUCCES: 'DELETE_DATA_SUCCES',
  ADD_DATA: 'ADD_DATA',
  ADD_DATA_SUCCESS: 'ADD_DATA_SUCCES',
  HYDRATE: 'HYDRATE',
  SHOW_MODAL: 'SHOW_MODAL',
  HIDE_MODAL: 'HIDE_MODAL',
  UPDATE_DATA: 'UPDATE_DATE',
  UPDATA_DATA_SUCCESS: 'UPDATE_DATA_SUCCESS',
}

// Load Data Action
export function loadData() {
  return { type: actionTypes.LOAD_DATA }
}
export function loadDataSuccess(data) {
  return {
    type: actionTypes.LOAD_DATA_SUCCESS,
    data,
  }
}

// Delete Data Action
export function deleteData(id) {
  return {
    type: actionTypes.DELETE_DATA,
    id
  }
}
export function deleteDataSuccess(id) {
  console.log("Action ID", id)
  return {
    type: actionTypes.DELETE_DATA_SUCCES,
    id
  }
}

//Addnew Data Action

export function addNew(data) {
  console.log("User Action", data)
  return {
    type: actionTypes.ADD_DATA,
    data
  }
}
export function addNewSucces(data) {
  console.log("User Action Success", data)
  return {
    type: actionTypes.ADD_DATA_SUCCESS,
    data
  }
}

// Model Action

export function hideModal() {
  return {
    type: actionTypes.HIDE_MODAL,
  }
}

export function showModal(id) {
  return {
    type: actionTypes.SHOW_MODAL,
    id
  }
}

//Update Data Action

export function updateData(data) {
  return {
    type: actionTypes.UPDATE_DATA,
    data,
  }
}

export function updateDataSuccess(data) {
  return {
    type: actionTypes.UPDATA_DATA_SUCCESS,
    data,
  }
}