import { actionTypes } from './actions'
import { HYDRATE } from 'next-redux-wrapper'
import { call } from '@redux-saga/core/effects';

const initialState = {
  placeholderData: [],
  isShow: false,
  idUpdate: null,
}

function MainReducer(state = initialState, action) {
  switch (action.type) {
    case HYDRATE:
      return {
        ...state,
        ...action.payload.data,
      }
    case actionTypes.LOAD_DATA: {
      return {
        ...state,
      }
    }
    case actionTypes.HIDE_MODAL: {
      return {
        ...state,
        isShow: false,
      }
    }
    case actionTypes.SHOW_MODAL: {
      return {
        ...state,
        isShow: true,
        idUpdate: action.id
      }
    }
    case actionTypes.LOAD_DATA_SUCCESS:
      return {
        ...state,
        placeholderData: action.data,
      };
    case actionTypes.DELETE_DATA_SUCCES:
      return {
        ...state,
        placeholderData: state.placeholderData.filter(item => item.id !== action.id)
      };
    case actionTypes.UPDATA_DATA_SUCCESS:
      const newUpdate = {
        ...action.data
      }
      console.log("New Update => ", newUpdate)

      const findID = state.placeholderData.findIndex(item => item.id === newUpdate.id);
      console.log("Find ID => ", findID)
      console.log(" NewUpdate ID =>", newUpdate.id)

      state.placeholderData.splice(findID, 1, newUpdate)
      console.log("New Users =>", newUser)

      return {
        ...state,
        placeholderData: state.placeholderData
      }
    case actionTypes.LOAD_DATA_SUCCESS:
      return {
        ...state,
      }
    case actionTypes.ADD_DATA_SUCCESS:
      const data_object = action.data;

      console.log("Data Object", data_object)

      return {
        ...state,
        placeholderData: state.placeholderData.concat(data_object)
      };
    default:
      return state
  }
}

export default MainReducer
