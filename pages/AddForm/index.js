import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addNew } from '../../actions';
import styles from '../AddForm/addForm.module.css'
import { useRouter } from 'next/router';


const AddForm = () => {

    const dispatch = useDispatch();




    const [data, setData] = useState({
        name: "",
        email: "",
        username: "",
        phone: "",
    })
    const router = useRouter();

    const handleAddNew = () => {
        dispatch(addNew(data));
        router.push("/User")
    }
    return (
        <>
            <div className={styles.form}>
                <div className={styles.form_wrap}>
                    <div className={styles.flex}>
                        <div className={styles.form_title}>Add New</div>
                        <div className={styles.form_form}>
                            <div className={styles.form_main}>
                                <div className={styles.form_haft}>
                                    <input
                                        placeholder="Name"
                                        className={styles.form_input}
                                        value={data.input}
                                        onChange={(e) => setData({ ...data, name: e.target.value })} />
                                </div>
                                <div className={styles.form_haft}>
                                    <input
                                        placeholder="UserName"
                                        className={styles.form_input}
                                        value={data.input}
                                        onChange={(e) => setData({ ...data, username: e.target.value })} />
                                </div>
                                <div className={styles.form_haft}>
                                    <input
                                        placeholder="Email"
                                        className={styles.form_input}
                                        value={data.input}
                                        onChange={(e) => setData({ ...data, email: e.target.value })} />
                                </div>
                                <div className={styles.form_haft}>
                                    <input
                                        placeholder="Phone Number"
                                        className={styles.form_input}
                                        value={data.input}
                                        onChange={(e) => setData({ ...data, phone: e.target.value })} />
                                </div>
                            </div>
                        </div>
                        <button className={styles.button} onClick={handleAddNew}>Add</button>
                    </div>
                </div>
            </div>

        </>
    )
}

export default AddForm