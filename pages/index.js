import Link from 'next/link'
import styles from '../styles/home.module.css'


const Index = () => {

  return (
    <>
      <div className={styles.home}>

        <div className={styles.title}>
          Welcome to my NextJs Tutorial
        </div>

        <Link href="/User/">
          <a href="#" className={styles.main}>
            <span className={styles.span}>
              <div className={styles.spanone}> Discovery </div>
              <div className={styles.spantwo}> Discovery </div>
            </span>
          </a>
        </Link>
      </div>


    </>
  )
}

export default Index
