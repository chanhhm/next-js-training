import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { loadData, deleteData, showModal } from '../../actions';
import styles from './page.module.css';
import Link from 'next/link';
import { toast } from 'react-toastify';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { wrapper } from '../../store';
import { END } from '@redux-saga/core';
import { Modal } from '@material-ui/core';
import CreatModal from '../../components/Modal';

function Page() {

  const dispatch = useDispatch();
  useEffect(() => {

  }, [dispatch])

  const placeholderData = useSelector(state => state.data.placeholderData)


  const modal = useSelector(state => state.data.isShow);

  const idUpdate = useSelector(state => state.data.idUpdate);

  const handleDeleteData = (id) => {
    console.log(id)
    if (window.confirm("Are u sure about that?")) {
      dispatch(deleteData(id));
      toast.success("Delete Succes");
    }
  };

  const handleUpdateData = (id) => {
    dispatch(showModal(id));
  }

  return (
    <>
      <table className={styles.table}>
        <thead>
          <tr>
            <th>ID</th>
            <th className={styles.name}>Name</th>
            <th className={styles.name}>User Name</th>
            <th className={styles.name}>Email</th>
            <th className={styles.name}>Phone Number</th>
            <th className={styles.action}>Action</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {
            placeholderData.map(item => {
              return (
                <tr key={item.id}>
                  <th>{item.id}</th>
                  <td className={styles.name}>{item.name}</td>
                  <td className={styles.name}>{item.username}</td>
                  <td className={styles.name}>{item.email}</td>
                  <td className={styles.name}>{item.phone}</td>
                  <td className={styles.td__button}>
                    <button
                      className={styles.button_update}
                      onClick={() => handleUpdateData(item.id)}>
                      Update
                    </button>
                  </td>
                  <td><button onClick={() => handleDeleteData(item.id)} className={styles.button}>Delete</button></td>
                </tr>
              )
            })
          }
        </tbody>
      </table >
      <ToastContainer autoClose={1000} />
      <CreatModal modal={modal} idUpdate={idUpdate} />
    </>
  )
}




export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {

  if (!store.getState().placeholderData) {
    store.dispatch(loadData())
    store.dispatch(END)
  }

  await store.sagaTask.toPromise()
})

export default Page
