import { all, call, put, takeLatest } from 'redux-saga/effects'
import { actionTypes, addNew, addNewSucces, deleteDataSuccess, failure, hideModal, loadDataSuccess, showModal, updateDataSuccess } from './actions'
import { addUser, deleteUser } from './request'


function* loadDataSaga() {
  try {
    const res = yield fetch('https://jsonplaceholder.typicode.com/users')
    const data = yield res.json()
    yield put(loadDataSuccess(data))
    console.log(data)
  } catch (err) {
  }
}

function* deleteDataSaga({ id }) {
  console.log(id)
  try {
    yield put(deleteDataSuccess(id))
  } catch (error) {
  }
}

function* addDataSaga({ data }) {
  console.log("AddDataSaga => ", data)
  try {
    yield put(addNewSucces(data))
  } catch (error) {

  }
}

function* updateDataSaga({ data }) {
  const { id } = data;

  try {
    yield put(hideModal(id))
    yield put(updateDataSuccess(data));
  } catch (error) {
  }
}


function* rootSaga() {
  yield all([
    takeLatest(actionTypes.LOAD_DATA, loadDataSaga),
    takeLatest(actionTypes.DELETE_DATA, deleteDataSaga),
    takeLatest(actionTypes.ADD_DATA, addDataSaga),
    takeLatest(actionTypes.UPDATE_DATA, updateDataSaga)
  ])
}

export default rootSaga
