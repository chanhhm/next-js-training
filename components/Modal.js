import { Button, Input, Modal, TextareaAutosize, TextField } from '@material-ui/core';
import { useSelector, useDispatch } from "react-redux";
import { hideModal, updateData, updateDataSuccess } from '../actions';
import { useState } from 'react';


const CreatModal = ({ modal, idUpdate }) => {



    const dispatch = useDispatch();

    const handleSubmit = (e) => {
        e.preventDefault();
    }

    const handleCloseModal = () => {
        dispatch(hideModal());
    }

    const handleUpdateModal = () => {
        const newData = { id: idUpdate, ...data }
        dispatch(updateData(newData))
    }


    const [data, setData] = useState({
        name: "",
        username: "",
        email: "",
        phone: "",
    })


    const body = (
        <div className="paper">
            <h2 className="modal__name">Update Post</h2>
            <form className="form" onSubmit={handleSubmit}>
                <input
                    className="modal__input"
                    placeholder='Name'
                    onChange={(e) => setData({ ...data, name: e.target.value })}
                />
                <input
                    className="modal__input"
                    placeholder='UserName'
                    onChange={(e) => setData({ ...data, username: e.target.value })}
                />
                <input
                    className="modal__input"
                    placeholder='Email'
                    onChange={(e) => setData({ ...data, email: e.target.value })}
                />
                <input
                    className="modal__input"
                    placeholder='Phone'
                    onChange={(e) => setData({ ...data, phone: e.target.value })}
                />
                <div className="modal__footer">
                    <button
                        className="modal__button button_update"
                        onClick={handleUpdateModal}
                    >
                        Update
                    </button>
                    <button
                        className="modal__button button_cancel"
                        onClick={handleCloseModal}
                    >
                        Cancel
                    </button>
                </div>
            </form>
        </div>
    );

    return (
        <Modal open={modal}>
            {body}
        </Modal>
    )
}



export default CreatModal