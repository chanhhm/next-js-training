import Link from 'next/link';

const NavBar = () => {
    return (
        <header className="header">
            <div className="nav">
                <Link href="/">
                    <a className="nav-logo">Next <span>.Js</span></a>
                </Link>
                <ul className="nav-list">
                    <li className="nav-item">
                        <Link href="/">
                            <a className="nav-link">
                                Home
                            </a>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link href="/User">
                            <a className="nav-link">
                                User List
                            </a>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link href="/AddForm">
                            <a className="nav-link">
                                Add User
                            </a>
                        </Link>
                    </li>
                </ul>
            </div>
        </header>
    )
}
export default NavBar